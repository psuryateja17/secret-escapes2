@isTest
public class Test_PartnerStaging {
    public testmethod static void methodName(){
        
        list<PartnerStaging__c> listps = new list<PartnerStaging__c>();
        listps =  dataGenerator();

        Test.startTest();
            database.insert(listps);
            listps[0].processed__c = true;
            update listps[0];

            fetchChartData.barChartData();
            fetchChartData.filteredBarChartData('2020-04-01','2020-05-02');
        Test.stopTest();
        
    }

    public static list<PartnerStaging__c> dataGenerator(){

        list<product2> lstprd = new list<product2>();
        product2 prd = new product2();
        prd.name = 'Bronze';
        lstprd.add(prd);
        prd = new product2();
        prd.name = 'Gold';
        lstprd.add(prd);
        prd = new product2();
        prd.name = 'Silver';
        lstprd.add(prd);
        insert lstprd;

        list<PartnerStaging__c> lstPS = new list<PartnerStaging__c>();
        PartnerStaging__c ps = new PartnerStaging__c();
        ps.Address_Line_1__c = 'Test';
        ps.Address_Line_2__c = 'Test1';
        ps.Address_Line_3__c = 'Test2';
        ps.Country__c = 'UK';
        ps.County_State__c = 'London';
        ps.Customer_First_Name__c = 'PS0001';
        ps.Customer_Last_Name__c = 'LN0001';
        ps.Post_Code__c = 'SO12 IDF';
        ps.Subscription_currency__c = 'USD';
        ps.Subscription_tier__c = 'gold';
        ps.Subscription_Type__c = 'anual';
        ps.Subscription_Value__c = 100;
        lstPS.add(ps);

        for(integer i = 0 ; i<3000 ; i++){
            ps = new PartnerStaging__c();
            ps.Customer_First_Name__c = 'TestFN'+' '+string.valueOf(i);
            ps.Customer_Last_Name__c = 'TestLN'+' '+string.valueOf(i);
            ps.Subscription_Value__c = 100 + i;
            ps.Address_Line_1__c = 'Test'+' '+string.valueOf(i);
            ps.Address_Line_3__c = 'Test2'+' '+string.valueOf(i);
            if(math.mod(i,2) == 0){
                ps.Subscription_currency__c = 'USD';
                ps.Subscription_tier__c = 'gold';
                ps.Subscription_Type__c = 'anual';
            }
            else {
                ps.Subscription_currency__c = 'GBP';
                ps.Subscription_tier__c = 'bronze';
                ps.Subscription_Type__c = 'monthly';         
            }
            lstPS.add(ps);

        }
        return lstPS;
    }

}