import { LightningElement,track,wire,api } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import chartjs from '@salesforce/resourceUrl/chart2';
import getChartData from '@salesforce/apex/fetchChartData.barChartData';
import getFilteredData from '@salesforce/apex/fetchChartData.filteredBarChartData';

export default class LifetimeSpending extends LightningElement {        
@track dataResult;
@track error;
@track singleSubscription = {};
@track strtsearchKey='';
@track endsearchKey='';
@api showFilter

products=[];
chartValues=[];        
chart;
chartjsInitialized = false;

@wire(getChartData) AggregateResult({error,data}) {
    if (data) {                         
        this.dataResult = data;
        for(let i =0 ; i < this.dataResult.length ; i++){
            
            this.products.push(this.dataResult[i].pname);
            console.log('>>>Value : '+this.dataResult[i].cnt);
            this.chartValues.push(this.dataResult[i].cnt);
        }
        this.chart.update();
    } else if (error) {
        //this.dataResult = undefined;
        this.error = error;
        console.log('Error: ' + this.error);
    }        
}

@wire(getFilteredData, {startDt:'$strtsearchKey', endDt :'$endsearchKey'})
wiredBarData({error, data}){
    if(data){
        this.products = []; 
        this.chartValues = [];
        console.log('>>Data Result and Length >> '+data);
        let i=0;
        for(let key in data){
            console.log('>>>>'+data[key]);
            console.log('>>>> Val'+ key);
            this.products.push(key);
            this.chartValues.push(data[key]);
            
        }   
    }
}

renderedCallback() {
        if (!this.chartjsInitialized) {
            
            loadScript(this, chartjs)
                .then(() => {
                    this.chartjsInitialized = true;
                    
                    this._buildChart();
                })
                .catch(error => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: "Error Loading Chart JS",
                            message: error.message,
                            variant: "error"
                        })
                    );
                });
        }
        console.log('>>>renderedCallback done!');
}

_buildChart() {
    let canvas = this.template.querySelector("canvas");
    let context = canvas.getContext("2d");

    this.chart = new window.Chart(context, {
        type: "bar",
        data: {
            //labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"] ,
            labels: this.products,
            datasets: [
                {
                    label: "Subscription Count",
                    //data: [12, 19, 3, 5, 2, 3],
                    data: this.chartValues,
                    backgroundColor: [
                        "rgba(255, 99, 132, 0.2)",
                        "rgba(54, 162, 235, 0.2)",
                        "rgba(255, 206, 86, 0.2)"
                    ],
                    borderColor: [
                        "rgba(255, 99, 132, 1)",
                        "rgba(54, 162, 235, 1)",
                        "rgba(255, 206, 86, 1)"
                    ],
                    borderWidth: 1
                }
            ]
        },
        options: {
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true
                        }
                    }
                ]
            }
        }
    });
}

handleStrtChange(event){
    console.log('Inside strt change '+event.target.value);
    this.strtsearchKey = event.target.value;
     
}

handleEndChange(event){
    this.endsearchKey = event.target.value;
     
}

handleChartChange(event){
    console.log(event.target.label);
    this.chart.destroy();
        var inp=this.template.querySelectorAll("lightning-input");


        inp.forEach(function(element){
            if(element.name=="input1")
            this.strtsearchKey  = element.value;

            else if(element.name=="input2")
            this.endsearchKey =element.value;
        },this);
        //this.chart.update();
        loadScript(this, chartjs)
        .then(() => {
            this.chartjsInitialized = true;
            
            this._buildChart();
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: "Error Loading Chart JS",
                    message: error.message,
                    variant: "error"
                })
            );
        });
}

} //end export