//Written By : Surya Teja
//Date : 19th April 2020
//Version 1.0
//Purpose : Capture trigger instances and redirect them to appropriate trigger handlers.
trigger Trigger_PartnerStaging on PartnerStaging__c (before insert,after insert,before update, after update,before delete) {

    system.debug('op type'+Trigger.OperationType);

    //Since we do not want to Over Load the trigger lets do all operations in a handler class.
    switch on Trigger.OperationType{

        when BEFORE_INSERT{
            system.debug('>>Inside before insert');
        }
        when AFTER_INSERT{
            system.debug('>>Inside after insert');
            TriggerHandler_PartnerStaging.handleAfterInsert(trigger.newMap,trigger.oldMap);
        }
        when BEFORE_UPDATE{
            system.debug('>>Inside after insert');
        }
        when AFTER_UPDATE{
            system.debug('>>Inside after insert');
        }
    }
    
}